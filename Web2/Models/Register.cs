﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using MySql.Data.MySqlClient;
using System.Web.Mvc;


namespace Web2.Models
{

    public class RegDetail
    {
        public List<Register> Roles { get; set; }
    }

    

    public class Register
    {
        public Register()
        {
        }

        public Register(int id, string email, string password, string name, string passcode)
        {
            Id = id;
            Email = email;
            Password = password;
            Name = name;
            Passcode = passcode;
        }

        public int Id
        { get; set; }

        public string Email
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Passcode
        {
            get; set;
        }


        string connString = string.Format("Server=userdb.cbdrq6pvkvph.us-east-1.rds.amazonaws.com; database=db1; UID=admin; password=password; SslMode = none");

        //Creating function to insert details
        public string InsertRegDetails(Register obj)
        {
            MySqlCommand cmd;
            MySqlConnection cnn = new MySqlConnection(connString);

            //string insertQuery = "INSERT into user(Name,Email,Password) values ('" + obj.Name + "','" + obj.Email + "','" + obj.Password +"'); ";
            string insertQuery = "INSERT into users(Name,Email,Password) VALUES(@Name,@Email,@Password);"; 
            try
            {
                cnn.Open();
                cmd = new MySqlCommand(insertQuery,cnn);
                cmd.Parameters.AddWithValue("@Name", obj.Name);
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@Password", obj.Password);
               // cmd = new MySqlCommand(insertQuery, cnn);
                cmd.ExecuteNonQuery();
           
                cnn.Close();
                return "Success";
            }
            catch(Exception)
            {
                return "Error";
            }      
        }             
    }
}