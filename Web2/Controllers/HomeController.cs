﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Web2.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using Google.Authenticator;


namespace Web2.Controllers
{
    public class HomeController : Controller
    {
        string connString = string.Format("Server=userdb.cbdrq6pvkvph.us-east-1.rds.amazonaws.com; database=db1; UID=admin; password=password; SslMode = none");
        private const string key = "qaz12345!@@";

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var views = (int)(Session["ViewCount"] ?? 0) + 1;
            Session["ViewCount"] = views;
            ViewData["Message"] = views;

            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Title = "Login Page";
            
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.Title = "Register Page";
            return View();
        }

        public ActionResult Menu()
        {
            ViewBag.data = TempData["id"];
            ViewBag.email = TempData["email"];
            TempData.Keep();
            return View();
        }

        public ActionResult Logout()
        {
            TempData.Clear();
            ModelState.Clear();
            // return View("Login");
            return RedirectToAction("Login", "Home");
        }

        //browse to updatedetail page
        public ActionResult UpdateDetail()
        {
            //ViewBag.data = JsonConvert.SerializeObject(TempData["email"]);
            ViewBag.data = TempData["id"];
            TempData.Keep();
            return View();
        }

        //update  data
        [System.Web.Mvc.HttpPut]
        public ActionResult putDetail(string id)
        {
            var resumeDto = JsonConvert.DeserializeObject<Register>(id);
            MySqlCommand cmd;
            MySqlConnection cnn = new MySqlConnection(connString);
            string query = "UPDATE users SET Email=@Email , Name=@Name, Password=@Password where Id=@Id";
            try
            {
                cnn.Open();
                cmd = new MySqlCommand(query, cnn);
                cmd.Parameters.AddWithValue("@Name", resumeDto.Name);
                cmd.Parameters.AddWithValue("@Password", resumeDto.Password);
                cmd.Parameters.AddWithValue("@Email", resumeDto.Email);
                cmd.Parameters.AddWithValue("@Id", resumeDto.Id);
                cmd.ExecuteNonQuery();

                cnn.Close();
                return Json(new { success = true, responseText = "Your details have been updated" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, responseText = "Error Updating" }, JsonRequestBehavior.AllowGet);

            }
        }




        [System.Web.Mvc.HttpGet]
        public ActionResult getDetail(string id)
        {
            var resumeDto = JsonConvert.DeserializeObject<Register>(id);
            MySqlCommand cmd;
            MySqlConnection cnn = new MySqlConnection(connString);
            string query = "SELECT * FROM users WHERE Id=@Id";
            var model = new List<Register>();
            try
            {
                cnn.Open();
                cmd = new MySqlCommand(query, cnn);
                //cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@Id", resumeDto.Id);
                MySqlDataReader readsql = cmd.ExecuteReader();
                while(readsql.Read())
                {                 
                    var reg = new Register();
                    reg.Name = readsql["name"].ToString();
                    reg.Email = readsql["email"].ToString();
                    reg.Password = readsql["password"].ToString();
                    model.Add(reg);
                }
                return Json(new { data = model }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception)
            {
                return null;
            }
        }


        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> SetRegDetails(string model)
        {
            var resumeDto = JsonConvert.DeserializeObject<Register>(model);
            Register reg = new Register();
            reg.Name = resumeDto.Name;
            reg.Password = resumeDto.Password;
            reg.Email = resumeDto.Email;
            //reg.InsertRegDetails(reg);

            if (reg.InsertRegDetails(reg) == "Success")
            {
                //ModelState.Clear();
                string a = await Execute(reg.Email);
                if (a == "Success")
                {
                    return Json(new { success = true, responseText = "Registration Successful" }, JsonRequestBehavior.AllowGet);
                }
                else { return Json(new { success = false, responseText = "Connection Error. Please try again" }, JsonRequestBehavior.AllowGet); }

            }
            else
            {
                ModelState.Clear();
                return Json(new { success = false, responseText = "Connection Error. Please try again" }, JsonRequestBehavior.AllowGet);
            }
        }


        [System.Web.Mvc.HttpPost]
        public ActionResult SignIn(string model)
        {
            var detail = JsonConvert.DeserializeObject<Register>(model);
            MySqlCommand cmd;
            MySqlConnection cnn = new MySqlConnection(connString);
            string query = "SELECT * FROM users WHERE Email=@Email AND Password=@Password";
            var regModel = new List<Register>();
            try
            {
                cnn.Open();
                cmd = new MySqlCommand(query, cnn);
                cmd.Parameters.AddWithValue("@Email", detail.Email);
                cmd.Parameters.AddWithValue("@Password", detail.Password);
                MySqlDataReader readsql = cmd.ExecuteReader();
                if (readsql.HasRows)
                    {
                    while (readsql.Read())
                    {
                        var reg = new Register();
                        reg.Name = readsql["name"].ToString();
                        reg.Email = readsql["email"].ToString();
                        reg.Id = int.Parse(readsql["id"].ToString());
                        reg.Password = readsql["password"].ToString();
                        regModel.Add(reg);   
                    }
                   
                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                    Session["email"] = readsql["email"].ToString();
                    Session["id"] = readsql["id"].ToString();
                    string userUniqueKey = Convert.ToString(Session["email"]) + key;
                    Session["userUniqueKey"] = userUniqueKey;
                    var setupInfo = tfa.GenerateSetupCode("CSC Assignment", Convert.ToString(Session["email"]), userUniqueKey, 300, 300);
                    ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                    ViewBag.SetupCode = setupInfo.ManualEntryKey;
                    
                    return Json(new { success = true, url = Url.Action("Menu", "Home"), barcode = ViewBag.BarcodeImageUrl, setupCode = ViewBag.SetupCode }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "Invalid credentials" }, JsonRequestBehavior.AllowGet);
                }           
            }
            catch (Exception)
            {
                return null;
            }    
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Verify2Au(string model)
        {
            var detail = JsonConvert.DeserializeObject<Register>(model);
            var token = detail.Passcode;
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            string userUniqueKey = Convert.ToString(Session["email"]) + key;
            bool isValid = tfa.ValidateTwoFactorPIN(userUniqueKey, token);
            if (isValid)
            {
                TempData["id"] = Session["id"].ToString();
                TempData["email"] = Session["email"].ToString();


                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }


        static async Task<string> Execute(string email)
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("ngzhengning08@gmail.com", "Ng Zheng Ning");
            var subject = "Registration Successful";
            var to = new EmailAddress(email, "Anonymous");
            var plainTextContent = "Your registration with our application is successful";
            var htmlContent = "<p><strong>Thank you for registering with us.</strong></p> <p>Your registration application is successful!</p>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);

            return "Success";
        }



    }
}
